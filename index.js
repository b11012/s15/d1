// alert("Hello Again!");
	// Syntax: alert("message");

// Syntax, Statements and Comments
/*
	Statements- programming instructions that we tell our computer/ machine to perform

	Syntax- set of rules describing how statements must be constructed

	Comments- are parts of the code that is ignored
		>> meant to describe the written code
		>> two-types comments
			>> single line comments- double slash (//)
			>> multi-line comments- slash and asterisks
		>> command: ctrl + shift + / 

*/
// console.log = prints in the console
console.log("Hello World");

// Variables
	
	// Declaring variables
	// Any information that is used by an application is stored in what we call a "memory"
	// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
	// This makes it easier for us associate information stored in our devices to actual "names" about information
	/*
		Syntax: let/ const variableName;
	*/
	let myVariable;
	console.log(myVariable); //result: undefined

	let hello;
	console.log(hello); //result: not defined if a variable is not declared first
	// let hello; cannot access before initialization

	// Guides:
	/*
	1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
	2. Variable names should start with a lowercase character, use camelCase for multiple words.
	3. For constant variables, use the 'const' keyword.
	4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
	*/

//Declaring and initializing variables
	
	// Initializing variables - the instance when a variable is given it's initial/starting value

	/*
		Syntax: let/ const variableName = value;
	*/

//let variables- you can change the value of the variable
let productName = "desktop computer";
console.log(productName);

let price = 18999;
console.log(price);

//const variables- information / values that shouldn't be changed
// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended

const interest = 3.539;
console.log(interest);

//Reassigning variables
	
	// Reassigning a variable means changing it's initial or previous value into another value
/*
	Syntax: variableName = value;
*/
/*camel casing- lowercase letter then uppercase letter*/
productName = "laptop";
console.log(productName);

let friend = "Min";
friend = "Jane";
console.log(friend); //result: Jane

//let variable cannot be re-declared within its scope.
//let friend = "Kate";
//console.log(friend); //result: error: identifier friend has already been declared


// Values of constants cannot be changed and will simply return an error
// interest = 3.614;
// console.log(interest); //result: assignment to constant variable

let role = "Supervisor";
let name = "Edward";

/*
	Mini-Activity:
	1. print out the value of role and name
	2. re-assign the value of role to Director and print out the new role value
	3. send a screenshot of your console with the output
*/
console.log(role);
console.log(name);
role = "Director"
console.log(role);

// Reassigning variables vs. Initializing variables

//Declare a variable
let supplier;
// initialization- done after a variable has been declared
supplier = "Jane Smith Tradings";
console.log(supplier); //result: Jane Smith Tradings

// reassigning- done after giving initial value to the variable
supplier = "Zuitt Store";
console.log(supplier); //result: Zuitt Store

/*
const variable should be declared and initialized at the same time
const pi;
pi = 3.1416;
console.log(pi);*/ //result: error

// var vs. let/ const

// var- was used from 1997 to 2015
// let / const- ES6 updates (2015)

//what makes let/const different from var?

	//There are issues associated with variables declared with var, regarding hoisting.
	
	//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.

// Hoisting of var- JavaScript default behavior of moving declaration to the top
a = 5;
console.log(a); //result: 5
var a;

//let / const local and global scope
//Scope- essentially means where these variables are available for use

// global variable
// let outerVariable = "Hello";

//{
	// local variable
	// let / const are block scoped
	// block is a chunk of code surrouds by {}. A block lives in curly braces
	//let innerVariable = "Hello Again";
//};

//console.log(outerVariable); // result: hello
//console.log(innerVariable); // result: error

//same with const
	// const outerVariable = 'hello';

	// {
	//     const innerVariable = 'hello again'
	// }

	// console.log(outerVariable)
	// console.log(innerVariable) // innerVariable is not defined

// Multiple Variable Declarations
	// Though it is quicker to do without having to retype the "let" keyword, it is still best practice to use multiple "let"/"const" keywords when declaring variables
	// Using multiple keywords makes code easier to read and determine what kind of variable has been created

// let productCode = "DC017", productBrand = "Dell"; 
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);
//result: DC017, Dell

// const let = "hello";
// console.log(let); //result: error- let is a reserved keyword

// Data Types

// Strings
// series of alphanumeric characters that create a word, phrase, a sentence or anything related to text
// strings are wrapped around quotation marks ('') or ("")

let country = "Philippines";
let city = 'Caloocan City';
let numberString = "123456";

//Concatenating Strings
	// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = city + ',' + ' ' + country;
console.log(fullAddress); //result: Caloocan City, Philippines

/*
	Mini-Activity:
	1. initialize a variable called myName with your own name as a value
	2. Concatenate it with the phrase 'Hi I am'
	3. Store the concatenated strings in a variable called greeting
	4. Print out greeting in your console
	5. Send output in Hangouts
*/
//solution:
let myName = 'Tine';
let greeting = 'Hi I am' + ' ' + myName;
console.log(greeting);

//escape character (\)
	// The escape character (\) in strings in combination with other characters can produce different effects
	// "\n" refers to creating a new line in between text
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress); //result line break between MM and PH

// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character
let message = "John's father is coming home today";
console.log(message); //prints out the message

message = 'John\'s father is coming home tomorrow';
console.log(message); //prints out the message using escape character

let pokemon = "pikachu";
console.log(pokemon); //result: pillow

// Numbers
// Integers/ Whole Numbers
let count = 64;
console.log(count); //result: 64

// Decimal Numbers/ Fractions
let grade = 98.7;
console.log(grade); //result: 98.7

//Exponential Notations
let planetDistance = 2e10;
console.log(planetDistance); //result: 20000000000

// Concatenate text and numbers
console.log('Jino\'s grade last quarter is' + ' ' + grade);
// result: Jino's grade last quarter is 98.7

// consider where will you use it and if will use mathematical operations
let numberString1 = "09912345678";
let number = 4578;

// Boolean
// the values relate to a state of a certain thing
// this will be useful in future discussion considering logic

let isSingle = true;
let inGoodConduct = false;
console.log("isSingle" + ' ' + isSingle);
console.log("inGoodConduct" + ' ' + inGoodConduct);

// additional info:
	// 1 equivalent to true
	// 0 equivalent to false

// Arrays
	// Arrays are a special kind of data type that's used to store multiple values
	// Arrays can store different data types but is normally used to store similar data types

/*
Syntax:
	let / const arrayName = [elementA, elementB ...]
*/

let anime = ["Naruto", "Bleach", "Attack On Titan", "Demon Slayer", "Spy x Family"];
console.log(anime);

let quarterlyGrades = [95, 96.3, 87, 90];
console.log(quarterlyGrades);

// it works but doesn't make sense in the context programming and not recommended
let random = ["JK", 24, true];
console.log(random); 

// Objects
	// Objects are another special kind of data type that's used to mimic real world objects/items
	// They're used to create complex data that contains pieces of information that are relevant to each other
	// Every individual piece of information is called a property of the object
/*
	Syntax: 
	let/ const objectName = {
		propertyA: valueA,
		propertyB: valueB
	}
	key-value pairs
*/
let person = {
	fullName: "Midoriya Izuku",
	age: 15,
	isStudent: true,
	contactNo: ["091234567899", "8123 4567"],
	address: {
		houseNumber: "568",
		city: "Tokyo"
	}
};
console.log(person);

// They're also useful for creating abstract objects
let myGrades = {
    firstGrading: 98.7, 
    secondGrading: 92.1, 
    thirdGrading: 90.2, 
    fourthGrading: 94.6
}

console.log(myGrades);

/*
        Constant Objects and Arrays
            The keyword const is a little misleading.

            It does not define a constant value. It defines a constant reference to a value.

            Because of this you can NOT:

            Reassign a constant value
            Reassign a constant array
            Reassign a constant object

            But you CAN:

            Change the elements of constant array
            Change the properties of constant object

        */

        //for example:

        // const anime = ['one piece', 'one punch man', 'attack on titan'] 
        // anime = ['kimetsu no yaiba']

        // console.log(anime)// Assignment to constant variable.

// Null
// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
let spouse = null;
console.log(spouse);

// Using null compared to a 0 value and an empty string is much better for readability purposes
// null is also considered as a data type of it's own compared to 0 which is a data type of a number and single quotes which are a data type of a string
let emptyString = ""
console.log(emptyString);

let myNumber = 0;
console.log(myNumber);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing
